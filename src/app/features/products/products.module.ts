import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product/product.component';
import {ButtonModule} from "../../shared/button/button.module";
import {InputModule} from "../../shared/input/input.module";
import { ProductsComponent } from './products.component';



@NgModule({
  declarations: [
    ProductComponent,
    ProductsComponent
  ],
  exports: [
    ProductsComponent
  ],
  imports: [
    CommonModule, ButtonModule, InputModule
  ]
})
export class ProductsModule { }
